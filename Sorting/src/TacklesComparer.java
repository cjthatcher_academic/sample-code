
public class TacklesComparer implements Comparer {

	@Override
	public int compareTo(FootballPlayer fp1, FootballPlayer fp2) {
		if (fp1.getTackles() < fp2.getTackles()) return -1;
		if (fp1.getTackles() > fp2.getTackles()) return 1;
		return 0;
		
	}
	
}
