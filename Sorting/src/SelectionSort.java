import java.util.LinkedList;
import java.util.List;


public class SelectionSort {

	public LinkedList<FootballPlayer> sort(List<FootballPlayer> origList, Comparer comparer)
	{
		LinkedList<FootballPlayer> sortedList = new LinkedList<FootballPlayer>();
		LinkedList<FootballPlayer> oldList = new LinkedList<FootballPlayer>(origList);
		
		FootballPlayer min;
		
		for (int i = 0; i < origList.size(); i++)
		{
			min = oldList.get(0);
			
			for (FootballPlayer fp : oldList)
			{
				if (comparer.compareTo(fp, min) == -1)
				{
					min = fp;
				}
			}
			
			sortedList.add(min);
			oldList.remove(min);
		}
		
		return sortedList;
	}
	
//	private int compareTo(FootballPlayer fp1, FootballPlayer fp2)
//	{
//		if (fp1.getAge() < fp2.getAge()) return -1;
//		if (fp1.getAge() > fp2.getAge()) return 1;
//		return 0;
//	}
}