
public class FootballPlayer implements Comparable<FootballPlayer> {
	
	int age;
	long weightInGrams;
	int tackles;
	int pointsScored;
	String name;
	
	public FootballPlayer(int age, long weightInGrams, int tackles,
			int pointsScored, String name) {
		super();
		this.age = age;
		this.weightInGrams = weightInGrams;
		this.tackles = tackles;
		this.pointsScored = pointsScored;
		this.name = name;
	}
	
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public long getWeightInGrams() {
		return weightInGrams;
	}
	public void setWeightInGrams(long weightInGrams) {
		this.weightInGrams = weightInGrams;
	}
	public int getTackles() {
		return tackles;
	}
	public void setTackles(int tackles) {
		this.tackles = tackles;
	}
	public int getPointsScored() {
		return pointsScored;
	}
	public void setPointsScored(int pointsScored) {
		this.pointsScored = pointsScored;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "FootballPlayer [age=" + age + ", weightInGrams="
				+ weightInGrams + ", tackles=" + tackles + ", pointsScored="
				+ pointsScored + ", name=" + name + "]";
	}


	@Override
	public int compareTo(FootballPlayer o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
