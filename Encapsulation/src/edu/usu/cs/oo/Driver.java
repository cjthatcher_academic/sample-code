package edu.usu.cs.oo;

import edu.usu.cs.oo.math.MathDoer;

public class Driver {

	public static void main(String[] args)
	{
		MathDoer doer = new MathDoer();
		
		doer.add(2, 2);
		doer.add(20, 14);
		doer.subtract(doer.add(10, 11), doer.multiply(9, 2));
		
	}
}
