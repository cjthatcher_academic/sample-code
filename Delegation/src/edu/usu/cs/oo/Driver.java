package edu.usu.cs.oo;

import edu.usu.cs.oo.math.Calculator;
import edu.usu.cs.oo.math.Operation;
import edu.usu.cs.oo.student.Student;

public class Driver {
	
	public static void main(String[] args)
	{
		Student student = new Student(new Calculator() {

			@Override
			public int add(int a, int b) {
				return 100;
			}

			@Override
			public int subtract(int a, int b) {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int multiply(int a, int b) {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int mod(int a, int b) {
				// TODO Auto-generated method stub
				return 0;
			}
			
		}
		System.out.println(student.doMath(2, 10, Operation.ADD));
		System.out.println(student.doMath(2, 4, Operation.SUBTRACT));
		System.out.println(student.doMath(2, 4, Operation.MULTIPLY));
		System.out.println(student.doMath(2, 4, Operation.MOD));
		
	}

}
