package edu.usu.cs.oo.math;

public class DiscalculateCalculator implements Calculator{

	private Calculator simpleCalculator = new SimpleCalculator();
	
	@Override
	public int add(int a, int b) {
		return reverse(simpleCalculator.add(a, b));
	}

	@Override
	public int subtract(int a, int b) {
		return reverse(simpleCalculator.subtract(a, b));
	}

	@Override
	public int multiply(int a, int b) {
		return reverse(simpleCalculator.multiply(a, b));
	}

	@Override
	public int mod(int a, int b) {
		return reverse(simpleCalculator.mod(a, b));
	}
	
	private int reverse(int orig)
	{
		String s = Integer.toString(orig);
		char[] charArray = new char[s.length()];
		int index = 0;
		for (int i = s.length() - 1; i >= 0; i--)
		{
			charArray[index++] = s.charAt(i);
		}
		
		int reversedInt = Integer.parseInt(new String(charArray));
		return reversedInt;
	}

}
