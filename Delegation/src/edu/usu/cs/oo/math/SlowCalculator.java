package edu.usu.cs.oo.math;

public class SlowCalculator implements Calculator {

	@Override
	public int add(int a, int b) {
		try {
			Thread.sleep(1500);
			return a + b;
		}
		catch (Exception e){
			//ignore
		}

		return a + b;
	}

	@Override
	public int subtract(int a, int b) {
		try {
			Thread.sleep(1500);
		}
		catch (Exception e){
			//ignore
		}

		return a - b;
	}

	@Override
	public int multiply(int a, int b) {
		try {
			Thread.sleep(1500);
		}
		catch (Exception e){
			//ignore
		}

		return a * b;
	}

	@Override
	public int mod(int a, int b) {
		try {
			Thread.sleep(1500);
		}
		catch (Exception e){
			//ignore
		}

		return a % b;
	}

}
