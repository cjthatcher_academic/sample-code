package edu.usu.cs.oo.student;

import edu.usu.cs.oo.math.Calculator;
import edu.usu.cs.oo.math.Operation;

public class Student {
	
	private Calculator calculator;
	
	public Student(Calculator calculator)
	{
		this.calculator = calculator;
	}

	
	public int doMath(int a, int b, Operation operation)
	{
		if (operation.equals(Operation.ADD))
		{
			return calculator.add(a,b);
		}
	
		else if (operation.equals(Operation.SUBTRACT))
		{
			return calculator.subtract(a,b);
		}
		
		else if (operation.equals(Operation.MULTIPLY))
		{
			return calculator.multiply(a,b);
		}
		
		else if (operation.equals(Operation.MOD))
		{
			return calculator.mod(a,b);
		}
		
		return 0;
	}

}
